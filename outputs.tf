output "instance_id" {
  description = "IDs of the ec2 instances in the target group"
  value = module.alb.loadbalancer_dns
}