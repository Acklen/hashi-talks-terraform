provider "aws" {
  region     = var.aws_region
}

terraform {
  backend "s3" {
    #bucket is set in cli
    region = "us-east-1"
  }
}

module "network-sprint-0" {
  source  = "AcklenAvenue/modules/aws//modules/network"
  version = "0.0.102"
  vpc_cidr    = var.vpc_cidr
  name_prefix = var.name_prefix

  cidrs = {
    public1  = "10.0.1.0/24"
    public2  = "10.0.2.0/24"
    private1 = "10.0.3.0/24"
    private2 = "10.0.4.0/24"
    rds1     = "10.0.5.0/24"
    rds2     = "10.0.6.0/24"
  }
}

module "instance" {
  source            = "./modules/ec2"
  dev_ami           = var.dev_ami
  name_prefix       = "${var.name_prefix}-bastion"
  key_name          = var.key_name
  dev_instance_type = var.dev_instance_type
  public1_subnet_id  = module.network-sprint-0.public1_subnet_id
  project_prefix     = var.project_prefix
  # Security Group
  vpc_id =module.network-sprint-0.vpc_id
  #depends_on = [module.network-sprint-0]
}

module "alb" {
  source = "./modules/alb"
  name_prefix       = "${var.name_prefix}-alb"
  public1_subnet_id       = module.network-sprint-0.public1_subnet_id
  public2_subnet_id       = module.network-sprint-0.public2_subnet_id
  vpc_id                  = module.network-sprint-0.vpc_id
  healthy_threshold   = var.elb_healthy_threshold
  unhealthy_threshold = var.elb_unhealthy_threshold
  health_check_timeout             = var.elb_timeout
  health_check_interval            = var.elb_interval
  instance                = module.instance.instance_id
  #depends_on = [module.network-sprint-0]
}