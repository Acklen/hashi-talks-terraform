#NETWORK

variable "aws_region" {
  default = "us-east-1"
}
variable "name_prefix" {
  default = "Internal"
}
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}


#EC2

variable "project_prefix" {
  default = "hashi-talks"
}

variable "dev_instance_type" {
  default = "t2.small"
}

variable "dev_ami" {
  default = "ami-0dba2cb6798deb6d8"
}

variable "key_name" {
  default = "honorlock"
}

#ALB

variable "elb_healthy_threshold" {
  default = "3"
}

variable "elb_unhealthy_threshold" {
  default = "10"
}

variable "elb_timeout" {
  default = "5"
}

variable "elb_interval" {
  default = "10"
}
