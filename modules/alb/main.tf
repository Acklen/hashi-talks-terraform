resource "aws_alb" "application-loadbalancer" {
  name               = "${var.name_prefix}-app-lb"
  internal           = var.lb_internal
  load_balancer_type = var.lb_type

  subnets = [
    var.public1_subnet_id,
    var.public2_subnet_id
  ]

  security_groups = [aws_security_group.public_sg.id]

  enable_deletion_protection = var.enable_del_protection
  tags = {
    Name = "${var.name_prefix}-elb"
  }
}

resource "aws_alb_listener" "app-lb-listener-80" {
  load_balancer_arn = aws_alb.application-loadbalancer.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.app_lb_tgt.arn
  }
}

resource "aws_alb_target_group" "app_lb_tgt" {
  name     = "${var.name_prefix}-app-tgt-gp"
  port     = var.target_group_port
  protocol = var.target_group_protocol
  vpc_id   = var.vpc_id

  health_check {
    protocol            = var.health_check_protocol
    path                = var.health_check_path
    healthy_threshold   = var.healthy_threshold
    unhealthy_threshold = var.unhealthy_threshold
    timeout             = var.health_check_timeout
    interval            = var.health_check_interval
  }

  tags = {
    Name = "${var.name_prefix}-app-tgt-gp"
  }

}

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_alb_target_group.app_lb_tgt.arn
  target_id        = var.instance
  port             = 5000
}

#Public Security group

resource "aws_security_group" "public_sg" {
  name        = "${var.name_prefix}-public_sg"
  description = "Used for public and private instances for load balancer access"
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    for_each = var.public_sg_ingress
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  #Outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.public_egress_cidr
  }

  tags = {
    Name = "${var.name_prefix}-SG"
  }
}


