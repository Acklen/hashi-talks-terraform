output "loadbalancer_dns" {
  description = "IDs of the ec2 instances in the target group"
  value = aws_alb.application-loadbalancer.dns_name
}