variable "dev_instance_type" {
  default     = "t2.micro"
  description = "The type of instance to start"
}

variable "dev_ami" {
  description = "The AMI to use for the instance"
}

variable "name_prefix" {
  description = "tag name"
}
variable "project_prefix" {
  description = "tag project"
}

variable "key_name" {
  description = "The key name of the instance"
}

variable "public1_subnet_id" {
  description = "The VPC subnet ID"
}


variable "vpc_id" {
  description = "The VPC ID"
}

variable "public_sg_ingress" {
  type = list(
    object({
      from_port   = number
      to_port     = number
      protocol    = string
      cidr_blocks = list(string)
    })
  )
  default = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 5000
      to_port     = 5000
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  description = "Can be specified multiple times for each ingress rule"
}
variable "public_egress_cidr" {
  default     = ["0.0.0.0/0"]
  description = "CIDR blocks to allow outbound traffic"
}
