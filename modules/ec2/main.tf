data "template_file" "user_data" {
   template = file("${path.module}/user-data.sh")
}

resource "aws_security_group" "sg" {
  name        = "${var.name_prefix}-sg"
  description = "Security group for ${var.name_prefix}"
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    for_each = var.public_sg_ingress
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  #Outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.public_egress_cidr
  }

  tags = {
    Name    = "${var.name_prefix}-SG"
    Project = var.project_prefix
  }
}


resource "aws_instance" "ec2" {
  instance_type = var.dev_instance_type
  ami           = var.dev_ami
  user_data = data.template_file.user_data.rendered
  tags = {
    Name    = "${var.name_prefix}-EC2"
    Project = var.project_prefix
  }

  key_name                    = var.key_name
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = var.public1_subnet_id
  associate_public_ip_address = "true"

  depends_on = [aws_security_group.sg]
}

